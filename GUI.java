package scripts.red_WoadLeafBuyer;

import javax.swing.JFrame;
import javax.swing.JSlider;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

import scripts.red_WoadLeafBuyer.data.Variables;

import javax.swing.JLabel;
import java.awt.Font;



 
public class GUI {
		private Variables variables;
        private JFrame frame;
        /**
         * Create the application.
         */
        public GUI(Variables variables) {
        		this.variables = variables;
        		initialize();
        }
 
		/**
         * Launch the application.
        
        public static void main(String[] args) {
                EventQueue.invokeLater(new Runnable() {
                        public void run() {
                                try {
                                        GUI window = new GUI(variables);
                                        window.frame.setVisible(true);
                                } catch (Exception e) {
                                        e.printStackTrace();
                                }
                        }
                });
        }
 

        /**
         * Initialize the contents of the frame.
         */
        private void initialize() {
                frame = new JFrame();
                frame.setBounds(100, 100, 360, 420);
                frame.setResizable(false);
                frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                frame.getContentPane().setLayout(null);
               
                JLabel txtRedLeafBuyer = new JLabel("Red WoadLeafBuyer");
                txtRedLeafBuyer.setFont(new Font("Arial", Font.BOLD, 30));
                txtRedLeafBuyer.setHorizontalAlignment(SwingConstants.CENTER);
                txtRedLeafBuyer.setBounds(-2, 0, 359, 50);
                frame.getContentPane().add(txtRedLeafBuyer);
               
                JLabel txtMouseSpeed = new JLabel("Mouse Speed");
                txtMouseSpeed.setHorizontalAlignment(SwingConstants.CENTER);
                txtMouseSpeed.setFont(new Font("Arial", Font.PLAIN, 20));
                txtMouseSpeed.setBounds(-2, 70, 359, 20);
                frame.getContentPane().add(txtMouseSpeed);
               
                JSlider sliderMouseSpeed = new JSlider();
                sliderMouseSpeed.setPaintLabels(true);
                sliderMouseSpeed.setValue(100);
                sliderMouseSpeed.setPaintTicks(true);
                sliderMouseSpeed.setMajorTickSpacing(50);
                sliderMouseSpeed.setMaximum(300);
                sliderMouseSpeed.setMinimum(50);
                sliderMouseSpeed.setMinorTickSpacing(10);
                sliderMouseSpeed.setBounds(12, 90, 330, 60);
                frame.getContentPane().add(sliderMouseSpeed);
               
                JCheckBox chckbxAdvancedPaint = new JCheckBox("Advanced Paint");
                chckbxAdvancedPaint.setToolTipText("Wil be added soon");
                chckbxAdvancedPaint.setEnabled(false);
                chckbxAdvancedPaint.setBounds(197, 276, 145, 25);
                frame.getContentPane().add(chckbxAdvancedPaint);
                
                JButton btnStart = new JButton("Start");
                btnStart.setBounds(70, 347, 97, 25);
                frame.getContentPane().add(btnStart);
                btnStart.addActionListener(new ActionListener() {
                	public void actionPerformed(ActionEvent e) {
                		variables.setMouseSpeed(sliderMouseSpeed.getValue());
                		variables.setGUI(false);
                		//variables.setMaxBuy(Integer.valueOf(textField.getText()));
                         
                            frame.setVisible(false);
                           
                    }
                });
                
               
                JButton btnCancel = new JButton("Cancel");
                btnCancel.setBounds(190, 347, 97, 25);
                frame.getContentPane().add(btnCancel);
                btnCancel.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                    	variables.setGUI(false);
                        frame.setVisible(false);
                    }
                });           
        }
 
        public void setVisible(boolean b) {
                frame.setVisible(b);
               
        }
}