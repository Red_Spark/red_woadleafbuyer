package scripts.red_WoadLeafBuyer.tasks;


import org.tribot.api.General;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.NPCChat;
import org.tribot.api2007.types.RSInterface;
import org.tribot.api2007.types.RSItem;

public class BuyLeaf extends Task{
	private boolean DEBUG = false;
	private TaskManager manager;
	public BuyLeaf(TaskManager manager){
		this.manager = manager;
	}

	public void execute() {

		RSItem [] invItems = Inventory.find("Coins");
		if(invItems == null || invItems.length == 0){
			manager.variables.setRun(false);
			General.println("No more gold");
		}else if (invItems.length != 0){
			if(invItems[0].getStack() < 20){
				manager.variables.setRun(false);
				General.println("No more gold");
			}
		}


		RSInterface chatInterface;
		chatInterface = Interfaces.get(231);
		if(chatInterface != null && !chatInterface.isHidden()){
			if(!clickingChatOptions(1))	
				return;
		}
		chatInterface = Interfaces.get(217);
		if(chatInterface != null && !chatInterface.isHidden()){
			if(!clickingChatOptions(2))	
				return;
		}
		chatInterface = Interfaces.get(219);
		if(chatInterface != null && !chatInterface.isHidden()){
			clickingChatOptions(3);
		}
		
	}
	private boolean clickingChatOptions(int option){
		switch(option){
		case(1):
			return gardenerTalking();
		case(2):
			return playerTalking();
		case(3):
			return conversationOptions();

		}
		
		return true;
		
	}
	private boolean gardenerTalking(){
		if(DEBUG)
			General.println("Selecting continue for gardener");
		return NPCChat.clickContinue(true);//click continue button 

	}
	private boolean playerTalking(){
		if(DEBUG)
			General.println("Selecting continue for player");
		return NPCChat.clickContinue(true);//click continue button, i know its the same as gardenerTalking()(but for future changes and better structure i made them separate)
	}
	private boolean conversationOptions(){
		if(DEBUG)
			General.println("Selecting conversationOptions");
		if(NPCChat.selectOption("Yes please, I need woad leaves.", true)){//click it if it is available :)
			if(DEBUG)
				General.println("Click on:Yes please, I need woad leave");		
			return true;
		}else if(NPCChat.selectOption("How about 20 coins?", true)){
			if(DEBUG)
				General.println("Click on:How about 20 coins?");	
			manager.variables.incramentLeafCount(2);//i know this might fail because the leaves are added after further in conversation, but fuck it(for now at least)
			return true;
		}
		return false;

		
	}
	
	//TODO check the interface
	//TODO figure out a way to scan for String in interface instead of hard coding ids
	/*
	 * Interface notes:
	 * Interface 231 -Gardener speaking - 1
	 * Interface 217 -Player speaking - 2
	 * Interface 219 -Conversation Options - 3
	 * 
	 * 
	 */

	public boolean validate() {
		if(DEBUG) {
			General.println(this.getClass().toString() + ": " + (NPCChat.getMessage() != null ||
					NPCChat.getOptions() != null));

		}
		return NPCChat.getMessage() != null ||
				NPCChat.getOptions() != null ;
	}

	@Override
	public int priority() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String status() {
		// TODO Auto-generated method stub
		return "Buying stuff";
	}
	

}
