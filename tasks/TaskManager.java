package scripts.red_WoadLeafBuyer.tasks;

/**
 * Created on 17/12/2015
 * Got the idea for this framework from Andrews script Netamis_Orb_Charger
 * Stores/sorts tasks
 */
import java.util.ArrayList;
import java.util.List;

import org.tribot.api.General;
import scripts.red_WoadLeafBuyer.data.Variables;



public class TaskManager {
	private final boolean DEBUG = false;
	public Variables variables;
	
	public TaskManager(Variables variables) {
        list = new ArrayList<>();
        this.variables = variables;
    }
	public final Task[] BUYING_LEAF = {new WalkToGardener(this), new OpenDoor(this), new TalkToGardener(this), new BuyLeaf(this)}; 

	private List<Task> list;

	
	//MAIN METHOS
    public void addTasks(Task... tasks) {
        for (Task task: tasks) {
            if (!list.contains(task)) {
            	if(DEBUG)
            		General.println("Task added");
                list.add(task);
            }
        }
    }

    public void removeTask(Task task) {
        if (list.contains(task)) {
        	if(DEBUG)
        		General.println("Task removes");
            list.remove(task);
        }
    }

    public void clearTasks() {
    	if(DEBUG)
    		General.println("Task cleared");
        list.clear();
    }

    public int size() {
        return list.size();
    }

    /*
    Return the highest priority valid task.
     */
    public Task getValidTask() {
        if (list.size() > 0) {
        	for(int i = 0; i < list.size(); i++){
        		if(list.get(i).validate()){
        			return list.get(i);
        		}  
        		General.sleep(20, 40);
        	}        
        }
        return null;
    }
    /*
    Overridden compare method from the Comparator interface used by the
    Collections sort method to determine what task is at the head of the priority.
    Added to the standard compare method is a check to see if each
    task validates or not before comparing them.
    If one task does not validate and the other does, the task that validates
    assumes higher priority.
    Refer to the javadoc for the Comparator interface for an explanation of the
    return values from this method.
     */
    
    
    public int compare(Task o1, Task o2) {
        boolean o1Valid = o1.validate(), o2Valid = o2.validate();
        if (!o1Valid && o2Valid) return 1;
        else if (o1Valid && !o2Valid) return -1;
        return o2.priority() - o1.priority();
    }
    
    //END OF MAIN METHOS
    public boolean setStageTasks(String stage){
    	addTasks(BUYING_LEAF);
    	return true;
	}
	//TODO
//	private boolean getOrganized(){
//		return false;
//	}

}
