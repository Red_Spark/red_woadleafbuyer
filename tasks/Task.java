package scripts.red_WoadLeafBuyer.tasks;

public abstract class Task {
    public abstract void execute();

    public abstract boolean validate();

    public abstract int priority();

    public abstract String status();
}
