package scripts.red_WoadLeafBuyer.tasks;


import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.Player;
import org.tribot.api2007.ext.Doors;
import org.tribot.api2007.types.RSNPC;
import org.tribot.api2007.types.RSTile;

public class OpenDoor extends Task{
	private boolean DEBUG = false;
	private TaskManager manager;
	public OpenDoor(TaskManager manager){
		this.manager = manager;
	}
	public void execute() {

		if(Doors.handleDoorAt(new RSTile(3026, 3379), true)){
			General.println("We got the door");
			Timing.waitCondition(new Condition(){
				public boolean active() {
					return Doors.isDoorAt(new RSTile(3026, 3379), true);
				}
				
			}, General.random(1000, 2000));
		}
		
		
	}

	@Override
	public boolean validate() {
		if(DEBUG) {
			General.println("OpenDoor Validate:" + (Doors.isDoorAt(new RSTile(3026, 3379), false)));
			//This shit returns true is the door is closed <_<.If i set it to true=it always returns false FTW?
			//General.println(Doors.isDoorAt(new RSTile(3026, 3379), false));
		}
		RSNPC[] wyson = NPCs.find(manager.variables.getFarmerName());
		if(wyson.length == 0 || wyson[0] == null)//o no we can't wind wyson :)
			return false;
		RSTile wysonPos = wyson[0].getPosition();
		RSTile playerPos = Player.getPosition();
		if(wysonPos == null || playerPos ==null)//well fuck, why the hell are they null :)
			return false;
		return !manager.variables.inTheSameArea(playerPos, wysonPos) && Doors.isDoorAt(new RSTile(3026, 3379), false);//checks if door is opened or closed
	}

	@Override
	public int priority() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String status() {
		// TODO Auto-generated method stub
		return "Opening Door";
	}

}
