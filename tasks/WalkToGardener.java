package scripts.red_WoadLeafBuyer.tasks;

import org.tribot.api.General;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.Walking;
import org.tribot.api2007.ext.Doors;
import org.tribot.api2007.types.RSNPC;
import org.tribot.api2007.types.RSTile;

import scripts.redUtil.api.walking07.Walking07;



public class WalkToGardener extends Task {
	private final Boolean DEBUG = false;

	private TaskManager manager;
	RSNPC[] wyson;
	public  WalkToGardener(TaskManager manager){
		this.manager = manager;
	}

	public void execute() {
		walkToWyson();
		
	}

	private boolean walkToWyson() {

		if(Doors.isDoorAt(new RSTile(3026, 3379), false)){
			RSTile[] path = Walking.generateStraightPath(new RSTile(3026, 3379));
			if(path.length != 0){
				Walking07.walkPath(path, 0, new Condition(){//walk to him :)
					public boolean active() {
						return wyson[0].isOnScreen();
					}
				});
			}
		}




		RSTile[] path = Walking.generateStraightPath(wyson[0].getPosition());//gets wyson position and generates a path
		if(path.length == 0)
			return false;
		return Walking07.walkPath(path, 2, new Condition(){//walk to him :)
			public boolean active() {
				return wyson[0].isOnScreen();
			}
		});
		
	}
	public boolean validate() {
		wyson = NPCs.find(manager.variables.getFarmerName());// find the gardener	
		if(wyson.length > 0 && wyson[0] != null) {

			General.println(this.getClass().toString()+": "+ ( !wyson[0].isOnScreen()));
			return !wyson[0].isOnScreen();
		}else if(DEBUG){
			General.println(this.getClass().toString()+": false");
		}

		return false;//will add a webWalk to the garden itself if we are not in it;
	}

	@Override
	public int priority() {//ignore the these
		// TODO Auto-generated method stub
		return 0;
	}
	public String status() {
		// TODO Auto-generated method stub
		return "Walking to Gardener";
	}

}
