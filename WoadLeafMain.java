package scripts.red_WoadLeafBuyer;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.text.DecimalFormat;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.input.Mouse;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Camera;
import org.tribot.api2007.Login;
import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.Ending;
import org.tribot.script.interfaces.Painting;

import scripts.redUtil.api.GE07.GE_Check_Official;
import scripts.red_WoadLeafBuyer.data.Variables;
import scripts.red_WoadLeafBuyer.tasks.Task;
import scripts.red_WoadLeafBuyer.tasks.TaskManager;

@ScriptManifest(authors = "Red_Spark", category = "Money Making", name = "Red_WoadLeafBuyer", version = 1.1)
public class WoadLeafMain extends Script implements Painting, Ending{
	
	final ScriptManifest MANIFEST = (ScriptManifest)this.getClass().getAnnotation(ScriptManifest.class);
	public final String TITLE = MANIFEST.name()+" "+MANIFEST.version()+" by Red_Spark";
	private final boolean DEBUG = false;
	private Variables variables = new Variables();
	private TaskManager manager;
	@Override
	public void onEnd() {
		General.println("----------------------------------------------------------------");
		General.println("Stoped after:"+Timing.msToString(timeRan));
		General.println("Got:"+variables.getLeafCount()+" Woad Leaves");
		General.println("Profit:"+(LEAF_PRICE)*variables.getLeafCount());
		General.println("----------------------------------------------------------------");
		
	}

	long timeRan;
	public static final long startTime = System.currentTimeMillis();
    final int LEAF_PRICE = GE_Check_Official.getPrice(1793);//TODO FIX THIS
	public void onPaint(Graphics g) {
		DecimalFormat df = new DecimalFormat("###,###,###,###,###,###,###");
		String hourlyProfitString;
		String totalProfitString;
		String hourlyLeafString;
		String totalLeafString;
		timeRan = System.currentTimeMillis() - startTime;

		
        g.setFont(new Font("Verdana", Font.BOLD, 20));
        g.setColor(new Color(255, 0, 51));
        g.drawString(TITLE, 10, 40);
       
        g.setFont(new Font("Verdana", Font.BOLD, 14));
        g.drawString("Time Running:"+ Timing.msToString(timeRan), 10, 60); 
        
        totalLeafString = df.format(variables.getLeafCount());
        g.drawString("Leaves got:"+ totalLeafString, 10, 80);
        
        hourlyLeafString = df.format(((long)variables.getLeafCount() * 3600000 / timeRan));
        g.drawString("Leaves got P/H:"+hourlyLeafString, 10, 100);
        
        hourlyProfitString = df.format((long)variables.getLeafCount() * 3600000 / timeRan * (LEAF_PRICE-10));
        g.drawString("Profit P/H:"+hourlyProfitString, 10, 120);
        
        totalProfitString = df.format((LEAF_PRICE-10)*variables.getLeafCount());
        g.drawString("Total Profit:"+totalProfitString, 10, 140);
       
		
	}

	public void run() {
		loop(20,50);
		
	}

	private void loop(int min, int max) {
		onStart();
		manager = new TaskManager(variables);
		while(variables.run()){
			if(variables.areWeStuck()){//checks to see if we are stuck on a task
				General.println("We are stuck, reseting tasks!");
				General.println("If you see this message too often please");
				General.println("Contact Red_Spark and \"Print Script Stack Trace\"");
				variables.setGetNewTasks(true);
			}
			if(variables.getNewTasks()){	
				if(DEBUG)
					General.println("Getting new tasks");
				manager.clearTasks();
				manager.setStageTasks(variables.getStage());
				if(DEBUG)
					General.println("For stage:"+variables.getStage());
				
				variables.setGetNewTasks(false);
			}
			Task task = manager.getValidTask();
            if (task != null) {
    
                task.execute();
                sleep(min,max);
            }else{
            	if(DEBUG)
            		General.println("Task == null");
            	variables.setGetNewTasks(true);
            }
		}
		while(!Login.logout()){
			Login.logout();
			Timing.waitCondition(new Condition(){
				public boolean active() {
					return Login.logout();
				}}, General.random(2000, 3000));
		}
		
	}

	private void onStart() {
		General.println("----------------------------------------------------------------");
		GUI gui = new GUI(variables);
        gui.setVisible(true);
        while(variables.guiOn()){//Waiting for the user to input all GUI settings
                sleep(500);
        }  
        General.println("Mouse Speed set to:"+variables.getMouseSpeed());
        Mouse.setSpeed(variables.getMouseSpeed());
        
		
		while(Camera.getCameraAngle() != 100)
			Camera.setCameraAngle(100);
		General.println("----------------------------------------------------------------");
		
			
		
	}

}
